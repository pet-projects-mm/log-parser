import re

#File
with open('logfile.log') as log_file:
    log_lines = [line.rstrip() for line in log_file]

#Reminder! check Regular Expression with https://regex101.com/
re_pattern = r"(\d{2}/\d{2}) (\d{2}:\d{2}:\d{2}) (\w+)\s*:\s*(.*)"

for log_line in log_lines:
    match = re.match(re_pattern, log_line)
    if match:
        date = match.group(1)
        time = match.group(2)
        priority = match.group(3)
        info = match.group(4)
        print(f"Date: {date}")
        print(f"Time: {time}")
        print(f"Priority: {priority}")
        print(f"Info: {info}")